package com.example.tp_final_tnt

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt.databinding.FragmentPrincipalDestBinding
import com.example.tp_final_tnt.dummy.Plato
import com.example.tp_final_tnt.dummy.PlatoHistorial
import com.example.tp_final_tnt.historial.HistorialViewModel
import com.example.tp_final_tnt.plato.PlatoAdapter
import com.example.tp_final_tnt.plato.PlatoListener
import com.example.tp_final_tnt.plato.PlatoViewModel
import com.example.tp_final_tnt.plato.TipoPlato.ALMUERZO
import com.example.tp_final_tnt.plato.TipoPlato.DESAYUNO
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [Principal.newInstance] factory method to
 * create an instance of this fragment.
 */
class Principal : Fragment() {

    private lateinit var _binding: FragmentPrincipalDestBinding
    private val binding get() = _binding
    lateinit var recyclerView :RecyclerView
    private val platoViewModel: PlatoViewModel by activityViewModels()
    private val historialViewModel: HistorialViewModel by activityViewModels()
    private lateinit var database: DatabaseReference
    val args: PrincipalArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        crearCanal(getString(R.string.notification_channel_id), getString(R.string.notification_channel_name))
        initDatabase()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this._binding = FragmentPrincipalDestBinding.inflate( inflater, container, false)
        binding.platoViewModel = platoViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        realizarBindings()
        return binding.root
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        platoViewModel.platos.observe(viewLifecycleOwner, Observer { platos ->
            (recyclerView.adapter as PlatoAdapter).setPlatos(platos, platoViewModel.tipoPlato)
        })
    }



    private fun initDatabase() {
        database = FirebaseDatabase.getInstance().reference
        GlobalScope.launch { initPlatosDesdeBD() }
        GlobalScope.launch { initHistorialUsuarioDesdeBD() }
        GlobalScope.launch { initViandasDesdeBD() }
    }

    suspend private fun initPlatosDesdeBD() {
        database.child("menus").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
                mostrarMensaje("No pudimos traer los platos. Vuelve a iniciar al app")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    crearListaPlatos(dataSnapshot)
                    platoViewModel.platosCargados = true
                    checkearNotificacionMenuDiario()
                }
            }
        })
    }

    private fun initViandasDesdeBD() {
        val fechaInicio = getFechaInicioViandasStr()
        val fechaTope = getFechaTopeViandasStr()
        database.child("viandas").orderByChild("fecha").startAt(fechaInicio).endAt(fechaTope).limitToFirst(5).addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onCancelled(databaseError: DatabaseError) {
                mostrarMensaje("No pudimos traer las viandas. Vuelve a iniciar al app")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    crearListaViandas(dataSnapshot)
                }
            }
        })
    }


    private suspend fun initHistorialUsuarioDesdeBD() {
        val uid = args.uid
        database.child("users").child(uid).child("historial").addValueEventListener(object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                mostrarMensaje("No pudimos traer tu historial")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    cargarHistorialCompras(dataSnapshot)
                    if (!historialViewModel.historialCargado) {
                        historialViewModel.historialCargado = true
                        checkearNotificacionMenuDiario()
                    }
                }
            }
        })
    }

    fun crearListaPlatos(dataSnapshot: DataSnapshot): Boolean {
        val tipoPlato = dataSnapshot.children
        val hoy = getFechaHoyStr()
        //val almuerzoRef = dataSnapshot.child("almuerzo").children.child("fecha").equalsTo(hoy)
        tipoPlato.forEach {
            when (it.key?.toUpperCase()) {
                ALMUERZO.toString() -> {
                    val platosAlmuerzo = it.children.filter { plato -> plato.child("fecha").value == hoy }
                    platosAlmuerzo.forEach {platoSnapshot ->
                        if (platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
                    }
                }
                else -> {
                    it.children.forEach {platoSnapshot ->
                        if (platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
                    }
                }
            }
        }
        return true
    }

    private fun crearListaViandas(dataSnapshot: DataSnapshot) {
        Log.i("platos","==>VIANDAS con fechas calculadas >>> "+dataSnapshot.toString())
        val viandas = dataSnapshot.children
        viandas.forEach {platoSnapshot ->
            if(platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
        }

    }

    private fun getFechaHoyStr(): String {
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())
        return currentDate
    }

    private fun getFechaInicioViandasStr():String {
        val sdf = SimpleDateFormat("dd/M/yyyy")
        var cal = Calendar.getInstance()
        val delta = getDeltaTopeInicio()
        cal.add(Calendar.DAY_OF_WEEK, delta)
        return sdf.format(cal.time)
    }

    private fun getFechaTopeViandasStr():String {
        val CORRIMIENTO_DIAS = 1
        val sdf = SimpleDateFormat("dd/M/yyyy")
        var cal = Calendar.getInstance()
        val delta = getDeltaTopeInicio() + CORRIMIENTO_DIAS
        cal.add(Calendar.DAY_OF_WEEK, delta)
        return sdf.format(cal.time)
    }

    private fun getDeltaTopeInicio(): Int {
        val hoy =  Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        return when(hoy) {
            7 -> 0 //sabado, no restes dias
            else -> -1 * hoy
        }
    }


    private fun cargarHistorialCompras(dataSnapshot: DataSnapshot) {
        val platosHistorial = dataSnapshot.children
        platosHistorial.forEach {
            val platoHistorial = it.getValue(PlatoHistorial::class.java)
            historialViewModel.addPlatoHistorial(platoHistorial as PlatoHistorial)
        }
    }



    private fun mostrarMensaje(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    private fun crearCanal(idCanal: String, nombreCanal: String) {
        // Crear canal de notificacion para versiones superiores a API 26.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val canalNotificacion = NotificationChannel(
                idCanal,
                nombreCanal,
                NotificationManager.IMPORTANCE_HIGH
            )
                .apply {
                    setShowBadge(true)
                    enableLights(true)
                    lightColor = Color.RED
                    enableVibration(true)
                    description = getString(R.string.notification_channel_description)
                }
            // Registrar el canal
            val notificationManager: NotificationManager? = getSystemService(requireContext(), NotificationManager::class.java)
            notificationManager?.createNotificationChannel(canalNotificacion)
        }
    }

    private fun checkearNotificacionMenuDiario() {
        if (historialViewModel.notificacionDiariaCheckeada) return
        if (!historialViewModel.historialCargado) return
        if (!platoViewModel.platosCargados) return
        historialViewModel.notificacionDiariaCheckeada = true
        historialViewModel.getPlatosFavoritos().forEach {plato ->
            val list = platoViewModel.getMenuDelDia().filter { p -> p.nombre == plato.nombre }
            if (list.isEmpty()) { return@forEach }
            else {
                notificar(getString(R.string.descripcion_notificacion_favoritos))
                return
            }
        }
    }

    private fun notificar(mensaje: String) {
        val notificationManager: NotificationManager? = ContextCompat.getSystemService(requireContext(), NotificationManager::class.java)
        notificationManager?.sendNotification(mensaje,requireContext())
    }

    private fun initRecyclerView() {
        recyclerView = binding.principalPlatosRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getPlatoAdapter()
    }

    private fun getPlatoAdapter(): PlatoAdapter {
        return PlatoAdapter(object: PlatoListener{
            override fun onAgregarAlCarritoClicked(plato: Plato) {
                binding.platoViewModel?.onAgregarAlCarrito(plato)
            }
        } )
    }

    private fun realizarBindings() {
        binding.historialButton.setOnClickListener { navegarHaciaHistorial() }
        binding.fabCarrito.setOnClickListener{ navegarHaciaCarrito()}
        // Boton temporal
        binding.signOut.setOnClickListener { signOut() }
    }

    private fun signOut(){
        FirebaseAuth.getInstance().signOut()
        navegarHaciaPortada()
    }

    private fun navegarHaciaPortada() {
        findNavController().navigate(R.id.action_principal_dest_to_portada_dest)
    }


    private fun navegarHaciaHistorial() {
        findNavController().navigate(R.id.action_principal_dest_to_historial_dest)
    }

    private fun navegarHaciaCarrito() {
        findNavController().navigate(R.id.action_principal_dest_to_carritoFragment)
    }

}
