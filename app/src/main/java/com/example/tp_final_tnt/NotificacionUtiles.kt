package com.example.tp_final_tnt

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

private val NOTIFICATION_ID = 0

fun NotificationManager.sendNotification(mensaje: String, applicationContext: Context) {
    val contentIntent = Intent(applicationContext, MainActivity::class.java)
    val contentPendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
/*
    val imagen = BitmapFactory.decodeResource(
        applicationContext.resources,
        R.drawable.ic_gorro_de_cocinero
    )*/

    val builder = NotificationCompat.Builder(applicationContext,
        applicationContext.getString(R.string.notification_channel_id))
        .setSmallIcon(R.drawable.ic_gorro_de_cocinero)
        .setContentTitle(applicationContext.getString(R.string.titulo_notificacion))
        .setContentText(mensaje)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setContentIntent(contentPendingIntent)
        .setAutoCancel(true)

    with(NotificationManagerCompat.from(applicationContext)) {
        notify(NOTIFICATION_ID, builder.build())
    }
}

fun NotificationManager.cancelNotifications() {
    cancelAll()
}
