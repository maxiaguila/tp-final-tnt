package com.example.tp_final_tnt.plato

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt.R
import com.example.tp_final_tnt.dummy.Plato
import kotlinx.android.synthetic.main.fragment_detalle_plato_dest.view.*


class PlatoAdapter(var platoListener: PlatoListener): RecyclerView.Adapter<PlatoAdapter.PlatoViewHolder>() {
    private lateinit var platos: MutableList<Plato>
    private lateinit var tipoPlato: TipoPlato


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlatoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_detalle_plato_dest, parent, false)
        return PlatoViewHolder(view, platoListener)
    }

    override fun onBindViewHolder(holder: PlatoViewHolder, position: Int) {
        val plato = this.platos[position]
        holder.descripcion.text = plato.descripcion
        holder.precio.text = "$ ${plato.precio}"
        holder.imagen.setImageResource(plato.imagen as Int)
        holder.btnAgregarAlCarrito.setOnClickListener {
            platoListener.onAgregarAlCarritoClicked(plato)
        }
        when(tipoPlato) {
            TipoPlato.VIANDAS -> {
                val ubicacion_plato_en_semana = position + 1
                holder.nombre.text =
                    plato.nombre?.let { tipoPlato.getString(it, ubicacion_plato_en_semana) }
                holder.btnAgregarAlCarrito.isEnabled = tipoPlato.isEnabled(ubicacion_plato_en_semana) && !plato.nombre.isNullOrEmpty()
            }
            else -> {
                holder.nombre.text = plato.nombre?.let { tipoPlato.getString(it) }
                holder.btnAgregarAlCarrito.isEnabled = tipoPlato.isEnabled()}
        }
    }


    fun setPlatos(platos: MutableList<Plato>, tipoPlato: TipoPlato) {
        this.platos = platos
        this.tipoPlato = tipoPlato
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = platos.size

    inner class PlatoViewHolder(view: View, var platoListener: PlatoListener): RecyclerView.ViewHolder(view) {
        private val vista: View = view
        val nombre: TextView
        var descripcion: TextView
        var imagen: ImageView
        var precio: TextView
        var btnAgregarAlCarrito: Button
        init {
            nombre = vista.nombre_textView
            descripcion = vista.descripcion_TextView
            imagen = vista.imagen_plato
            precio = vista.precio_textView
            btnAgregarAlCarrito = vista.agregar_al_carrito_chip
        }

        override fun toString(): String {
            return super.toString() + " '" + nombre.text + "'"
        }
    }

}

