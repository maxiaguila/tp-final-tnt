package com.example.tp_final_tnt.plato

import com.example.tp_final_tnt.dummy.Plato

interface PlatoListener {
    fun onAgregarAlCarritoClicked(plato: Plato)
}