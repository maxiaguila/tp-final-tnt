package com.example.tp_final_tnt.dummy

import com.google.firebase.database.Exclude

class PlatoHistorial (
    val idPlato: String? = "",
    val nombre: String? = "",
    val descripcion: String? = "",
    var favorito: Boolean? = false,
    val imagen: Int? = 0){

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "idPlato" to idPlato,
            "nombre" to nombre,
            "descripcion" to descripcion,
            "favorito" to favorito,
            "imagen" to imagen
        )
    }

}