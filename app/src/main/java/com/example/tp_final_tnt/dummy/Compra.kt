package com.example.tp_final_tnt.dummy

import com.example.tp_final_tnt.carrito.TipoEntregaPedido
import com.google.firebase.database.Exclude


class Compra (pedidos: List<Pedido>, user: User, tipoEntregaPedido: TipoEntregaPedido = TipoEntregaPedido.RETIRO_EN_LUGAR) {
    var listaPlatos: MutableMap<String,Int> = mutableMapOf()
    var precio: Double = 0.00
    var tipoEntrega = TipoEntregaPedido.RETIRO_EN_LUGAR
    var nombreCliente: String = ""
    var direccionEntrega: String = ""
    var completo = false

    init {
        pedidos.forEach { pedido ->
            listaPlatos.put(pedido.nombre.toString(), pedido.cantidad!!)
            precio = precio + pedido.precio?.times(pedido.cantidad!!)!!
        }
        nombreCliente = user.nombre.toString()
        setEntrega(tipoEntregaPedido, user)
    }


    fun setEntrega (tipo: TipoEntregaPedido, user: User) {
        val COSTO_DELIVERY_A_CASA = 100.00
        val COSTO_DELIVERY_A_OFICINA = 50.00

        tipoEntrega = tipo
        when (tipoEntrega) {
            TipoEntregaPedido.DELIVERY_A_CASA -> {
                precio = precio + COSTO_DELIVERY_A_CASA
                direccionEntrega = user.direccion.toString()
            }
            TipoEntregaPedido.DELIVERY_A_OFICINA -> {
                precio = precio + COSTO_DELIVERY_A_OFICINA
                direccionEntrega = user.oficina.toString()
            }
            else -> precio
        }
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "listaPlatos" to listaPlatos,
            "precio" to precio,
            "tipoEntrega" to tipoEntrega,
            "completo" to completo
        )
    }
}