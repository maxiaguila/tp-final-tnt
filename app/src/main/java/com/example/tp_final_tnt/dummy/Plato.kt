package com.example.tp_final_tnt.dummy

import com.google.firebase.database.IgnoreExtraProperties

// TODO: terminar de establecer imagenes en propiedad imagen:Int

@IgnoreExtraProperties
data class Plato(
    var id: String? = "",
    val nombre: String? = "",
    val descripcion: String? = "",
    val precio: Double? = 0.00,
    val tipo: String? = "",
    val imagen: Int? = 0){ }
