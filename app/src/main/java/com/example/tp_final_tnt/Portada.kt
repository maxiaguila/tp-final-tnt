package com.example.tp_final_tnt

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.tp_final_tnt.databinding.FragmentPortadaDestBinding
import com.example.tp_final_tnt.dummy.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private lateinit var auth: FirebaseAuth
private lateinit var database: DatabaseReference

/**
 * A simple [Fragment] subclass.
 * Use the [Portada.newInstance] factory method to
 * create an instance of this fragment.
 */
class Portada : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var _binding: FragmentPortadaDestBinding
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        auth = FirebaseAuth.getInstance()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        val uid = auth.currentUser?.uid.toString()
        database = FirebaseDatabase.getInstance().reference

        // Verificamos si ya estaba logueado
        if (currentUser != null) {
            // User is signed in
           // Toast.makeText(context, "Logueo Automatico", Toast.LENGTH_LONG).show()
            modeloUsuario(uid, database)
            navegarHaciaPrincipal(uid)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //inflater.inflate(R.layout.fragment_portada_dest, container, false)
        this._binding = FragmentPortadaDestBinding.inflate(inflater, container, false)
        val  view = binding.root


        binding.entrarButton.setOnClickListener { loginApp() }
        binding.registrarButton.setOnClickListener { navegarHaciaRegistrarse() }

        return view
    }

    private fun loginApp() {
        val email = _binding.email.text.toString().trim()
        val password = _binding.passw.text.toString()
        database = FirebaseDatabase.getInstance().reference

        if(email.isNotEmpty() && password.isNotEmpty()){
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val uid = auth.currentUser?.uid.toString()
                    Log.d("TAG", "SignInUserWhitEMailPassword:Succes")
                    val user = auth.currentUser
                    Toast.makeText(context, "Se ha logueado correctamente", Toast.LENGTH_LONG).show()
                    modeloUsuario(uid, database)
                    navegarHaciaPrincipal(uid)
                } else {
                    Log.w("TAG", "SignInUserWhitEMailPassword:Failure", task.exception)
                    Toast.makeText(context, "Loguin Failed", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    private fun modeloUsuario(uid:String, database:DatabaseReference) {
        database.child("users").child(uid).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val uid =
                        dataSnapshot.child("uid").getValue(
                            String::class.java
                        ).toString()
                    val nombre =
                        dataSnapshot.child("nombre").getValue(
                            String::class.java
                        ).toString()
                    val telefono =
                        dataSnapshot.child("telefono").getValue(
                            String::class.java
                        ).toString()
                    val direccion =
                        dataSnapshot.child("direccion").getValue(
                            String::class.java
                        ).toString()
                    val userViewModel: UserViewModel by activityViewModels()
                    userViewModel.usuario = User(uid = uid, nombre = nombre, telefono = telefono, direccion = direccion)
                }
                //ya tenemos los datos desde Firebase cargados en user:User
                //alerta(dataSnapshot)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("ERROR", "Error de lectura")
            }
        })
    }

    private  fun alerta(data:DataSnapshot) {
        // Creo una Alerta
        val builder = AlertDialog.Builder(context)
        builder.setTitle("DATOS:")
        builder.setMessage(data.value.toString())
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun navegarHaciaPrincipal(uid: String) {
        val action = PortadaDirections.actionPortadaDestToPrincipalDest(uid)
        findNavController().navigate(action)
    }

    private fun navegarHaciaRegistrarse() {
        findNavController().navigate(R.id.action_portada_dest_to_fragment_registro_dest)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment portada_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Portada().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
