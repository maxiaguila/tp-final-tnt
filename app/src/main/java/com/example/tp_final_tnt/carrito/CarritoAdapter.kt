package com.example.tp_final_tnt.carrito

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt.R
import com.example.tp_final_tnt.dummy.Pedido
import kotlinx.android.synthetic.main.fragment_detalle_plato_dest.view.*
import kotlinx.android.synthetic.main.fragment_lista_carrito.view.*


class CarritoAdapter(
    var listenerCantidadProductoButtons: CarritoClickListener
): RecyclerView.Adapter<CarritoAdapter.CarritoViewHolder>() {

    private lateinit var pedidos: MutableList<Pedido>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarritoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_lista_carrito, parent, false)
        return CarritoViewHolder(view,listenerCantidadProductoButtons)
    }

    override fun onBindViewHolder(holder: CarritoViewHolder, posicionProducto: Int) {
        val productoPedido: Pedido = this.pedidos[posicionProducto]
        holder.nombre.text = productoPedido.nombre
        holder.cantidad.text = productoPedido.cantidad.toString()
        holder.precio.text = "$ ${productoPedido.precio}"
        holder.imagen.setImageResource(productoPedido.imagen as Int)
        holder.btnIncrementarCantidad.setOnClickListener {
            listenerCantidadProductoButtons.onIncrementarCantidadProductoClicked(productoPedido)
        }
        holder.btnRestarCantidad.setOnClickListener {
            listenerCantidadProductoButtons.onRestarCantidadProductoClicked(productoPedido)
        }
    }

    override fun getItemCount(): Int = pedidos.size

    internal fun setPedidos(listaPedidos: MutableList<Pedido>) {
        this.pedidos = listaPedidos
        notifyDataSetChanged()
    }

    inner class CarritoViewHolder(view: View, listener: CarritoClickListener): RecyclerView.ViewHolder(view) {
        private val vista: View = view
        var nombre: TextView
        var precio: TextView
        var imagen: ImageView
        var cantidad: TextView
        var btnIncrementarCantidad: Button
        var btnRestarCantidad: Button
        init {
            nombre = vista.pedido_nombrePlato
            precio = vista.pedido_precioUnidad
            imagen = vista.pedido_ImageView
            cantidad = vista.pedido_cantidadProducto
            btnIncrementarCantidad = vista.incrementarCantidad_buttonPedido
            btnRestarCantidad = vista.restarCantidad_buttonPedido
        }
    }
}