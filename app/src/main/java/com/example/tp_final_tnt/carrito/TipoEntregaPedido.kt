package com.example.tp_final_tnt.carrito

enum class TipoEntregaPedido {
    DELIVERY_A_CASA,
    DELIVERY_A_OFICINA,
    RETIRO_EN_LUGAR
}