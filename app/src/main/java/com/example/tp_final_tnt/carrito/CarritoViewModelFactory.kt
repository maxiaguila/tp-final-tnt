package com.example.tp_final_tnt.carrito


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tp_final_tnt.dummy.Pedido
import com.example.tp_final_tnt.dummy.Plato

class CarritoViewModelFactory (private val platos: MutableList<Pedido>): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CarritoViewModel( platos ) as T
    }

}