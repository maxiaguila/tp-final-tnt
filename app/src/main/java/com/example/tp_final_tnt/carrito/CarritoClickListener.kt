package com.example.tp_final_tnt.carrito

import com.example.tp_final_tnt.dummy.Pedido

interface CarritoClickListener {
    fun onIncrementarCantidadProductoClicked(producto: Pedido)
    fun onRestarCantidadProductoClicked(producto: Pedido)
}