package com.example.tp_final_tnt.carrito

import android.app.NotificationManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt.UserViewModel
import com.example.tp_final_tnt.databinding.FragmentCarritoBinding
import com.example.tp_final_tnt.dummy.Pedido
import com.example.tp_final_tnt.dummy.PlatoHistorial
import com.example.tp_final_tnt.historial.HistorialViewModel
import com.example.tp_final_tnt.plato.PlatoViewModel
import com.example.tp_final_tnt.sendNotification
import com.google.firebase.database.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.Map


class CarritoFragment : Fragment() {

    private lateinit var _binding: FragmentCarritoBinding
    private val binding get() = _binding
    private val platoViewModel : PlatoViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    val historialViewModel: HistorialViewModel by activityViewModels()
    lateinit var recyclerView : RecyclerView

    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCarritoBinding.inflate(inflater, container, false)

        val platosEnCarrito : MutableList<Pedido> = platoViewModel.platosEnCarrito.value as MutableList<Pedido>
        val carritoViewModel : CarritoViewModel by viewModels { CarritoViewModelFactory(( platosEnCarrito ) ) }
        carritoViewModel.user = userViewModel.usuario
        _binding.carritoViewModel = carritoViewModel
        _binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        binding.carritoViewModel?.pedidos?.observe(viewLifecycleOwner, Observer {
            pedidos ->
                (recyclerView.adapter as CarritoAdapter).setPedidos(pedidos)
                platoViewModel.updatePlatosDelCarrito(pedidos)
        })
        binding.comprarButton.setOnClickListener { realizarCompra() }
    }

    private fun initRecyclerView() {
        recyclerView = binding.listaCarritoRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getCarritoAdapter()
    }


    private fun realizarCompra() {
        binding.carritoViewModel?.onClickComprar()
        if (binding.carritoViewModel?.pudeArmarCompra() as Boolean) {
            escribirPedidoEnBD()
        }
    }

    private fun escribirPedidoEnBD() {
        val key = database.child("pedidos").push().key
        if (key == null) {
            onCompraFallida("Tu compra no se pudo realizar correctamente")
            return
        }
        val compra = binding.carritoViewModel?.compra
        database.child("pedidos").child(key).setValue(compra)
            .addOnFailureListener { onCompraFallida("Tu compra no se pudo realizar correctamente") }
            .addOnSuccessListener {
                agregarCompraAlHistorial()
                GlobalScope.launch {  escucharNotificacionPedido(key) }
            }
    }

    private fun escucharNotificacionPedido(idCompra: String) {
        val ref = database.child("pedidos").child(idCompra).child("completo")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                return
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val pedidoCocinado: Boolean = dataSnapshot.value as Boolean
                if (pedidoCocinado) {
                    ref.removeEventListener(this)
                    notificar("Tu pedido ya esta listo!")
                }
            }
        })
    }

    private fun notificar(mensaje: String) {
        val notificationManager: NotificationManager? = ContextCompat.getSystemService(requireContext(), NotificationManager::class.java)
        notificationManager?.sendNotification(mensaje,requireContext())
    }

    private fun agregarCompraAlHistorial() {
        val platosComprados = binding.carritoViewModel!!.getPedidosConCantidadMayorACero()
        val listaPlatosParaAgregarAlHistorial = historialViewModel.transformaPedidosEnPlatosHistorial(platosComprados)
        if (listaPlatosParaAgregarAlHistorial.isEmpty()){
            onCompraExitosa()
        } else {
            agregarPlatosAHistorialUsuario(listaPlatosParaAgregarAlHistorial)
        }
    }

    private fun agregarPlatosAHistorialUsuario(platos: List<PlatoHistorial>) {
        platos.forEach { plato ->
            database.child("users").child(userViewModel.usuario.uid.toString()).child("historial").child(plato.idPlato.toString()).setValue(plato)
                .addOnFailureListener { onCompraFallida("Tu compra fue realizado, pero no se pudo agregar al historial") }
                .addOnSuccessListener { onCompraExitosa() }
        }
    }

    private fun onCompraExitosa() {
        binding.carritoViewModel?.onClickLimpiarCarrito()
        Toast.makeText(context, "Tu compra fue realizada correctamente", Toast.LENGTH_LONG).show()
    }

    private fun onCompraFallida(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    private fun getCarritoAdapter(): CarritoAdapter {
        return CarritoAdapter(object:CarritoClickListener{
            override fun onIncrementarCantidadProductoClicked(producto: Pedido) {
                binding.carritoViewModel?.onIncrementarCantidadProducto(producto)
                habilitarBotonesCompra()
            }

            override fun onRestarCantidadProductoClicked(producto: Pedido) {
                binding.carritoViewModel?.onRestarCantidadProducto(producto)
                habilitarBotonesCompra()
            }
        })
    }

    fun habilitarBotonesCompra() {
        binding.comprarButton.isEnabled = binding.carritoViewModel?.estaHabilitadoParaComprar()!!
        binding.limpiarCarritoButton.isEnabled = binding.carritoViewModel?.puedoLimpiarCarrito()!!
    }

}

