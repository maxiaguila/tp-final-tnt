package com.example.tp_final_tnt

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.tp_final_tnt.databinding.FragmentRegistroDestBinding
import com.example.tp_final_tnt.dummy.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private lateinit var auth: FirebaseAuth
private lateinit var database: DatabaseReference

/**
 * A simple [Fragment] subclass.
 * Use the [fragment_registro_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
@Suppress("UNREACHABLE_CODE")
class fragment_registro_dest : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var _binding: FragmentRegistroDestBinding
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        auth = FirebaseAuth.getInstance()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentRegistroDestBinding.inflate(inflater, container, false)
        val  view = binding.root

        binding.registrar.setOnClickListener { registrarUsuario() }

        return  view
    }

    private fun registrarUsuario() {

        val email = _binding.email.text.toString().trim()
        val password = _binding.password.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("TAG", "CreateUserWhitEMailPassword:Succes")
                    var user = auth.currentUser
                    Toast.makeText(context, "Su usuario ha sido creado correctamente", Toast.LENGTH_LONG).show()
                    val modeloUsuario = saveUserToFirebase()
                    alerta(modeloUsuario)
                    val userViewModel: UserViewModel by activityViewModels()
                    userViewModel.usuario = modeloUsuario
                    userViewModel.usuario.uid?.let { navegarHaciaPrincipal(it) }
                } else {
                    Log.w("TAG", "CreateUserWhitEMailPassword:Failure", task.exception)
                    Toast.makeText(context, "Authentication Failed", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    private fun saveUserToFirebase(): User {
        database = FirebaseDatabase.getInstance().reference
        val nombre = _binding.nombre.text.toString()
        val direccion = _binding.direccion.text.toString()
        val telefono = _binding.telefono.text.toString()
        val uid = auth.currentUser?.uid.toString()

        val user =
            User(uid = uid, nombre =  nombre, direccion =  direccion, telefono =  telefono)

        database.child("users").child(uid).setValue(user)

        return user
    }

    private  fun alerta(value: User) {
        // Creo una Alerta
        val nombre = value.nombre
        val uid = value.uid
        val telefono = value.telefono
        val direccion = value.direccion

        val builder = AlertDialog.Builder(context)
        builder.setTitle("DATOS:")
        builder.setMessage("$uid $nombre $direccion $telefono")
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun navegarHaciaPrincipal(uid: String) {
        val action = fragment_registro_destDirections.actionFragmentRegistroDestToPrincipalDest(uid)
        findNavController().navigate(action)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment_registro_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragment_registro_dest().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}




