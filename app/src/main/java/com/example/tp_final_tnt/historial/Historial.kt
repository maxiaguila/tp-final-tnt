package com.example.tp_final_tnt.historial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt.UserViewModel
import com.example.tp_final_tnt.databinding.FragmentHistorialDestBinding
import com.example.tp_final_tnt.dummy.PlatoHistorial
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [Historial.newInstance] factory method to
 * create an instance of this fragment.
 */
class Historial : Fragment() {

    private lateinit var _binding : FragmentHistorialDestBinding
    private val binding get() = _binding
    lateinit var recyclerView : RecyclerView
    private val historialViewModel: HistorialViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHistorialDestBinding.inflate(inflater, container, false)
        _binding.historialViewModel = historialViewModel
        _binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        historialViewModel.historial.observe(viewLifecycleOwner, Observer { historial ->
            (recyclerView.adapter as HistorialAdapter).setHistorial(historial)
        })
    }

    private fun initRecyclerView() {
        recyclerView = binding.historialRecyclerView
        recyclerView.layoutManager =  LinearLayoutManager(context)
        recyclerView.adapter = getAdapter()
    }

    fun getAdapter() : HistorialAdapter {

        return HistorialAdapter( object:HistorialListener{
            override fun onClickCheckboxFavorito( platoHistorial: PlatoHistorial, esFavorito: Boolean ) {
                //TODO: usar corrutinas con updateFavorito()
                //historialViewModel.onClickCheckboxFavorito(platoHistorial, esFavorito)
                //GlobalScope.launch { updateFavorito(platoHistorial, esFavorito) }
                updateFavorito(platoHistorial, esFavorito)
            }
        } )
    }

    //suspend fun  updateFavorito(plato: PlatoHistorial, esFavorito: Boolean) {
    fun  updateFavorito(plato: PlatoHistorial, esFavorito: Boolean) {
        val uid = userViewModel.usuario.uid.toString()
        val idPlato = plato.idPlato.toString()
        database.child("users").child(uid).child("historial").child(idPlato).child("favorito").setValue(esFavorito)
            .addOnSuccessListener { Toast.makeText(context,"Favorito actualizado",Toast.LENGTH_SHORT).show() }
        //Toast.makeText(context,esFavorito.toString(),Toast.LENGTH_SHORT).show()
    }
}