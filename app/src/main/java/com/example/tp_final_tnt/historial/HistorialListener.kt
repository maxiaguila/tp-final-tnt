package com.example.tp_final_tnt.historial

import com.example.tp_final_tnt.dummy.PlatoHistorial

interface HistorialListener {
    fun onClickCheckboxFavorito(platoHistorial: PlatoHistorial, esFavorito: Boolean)
}