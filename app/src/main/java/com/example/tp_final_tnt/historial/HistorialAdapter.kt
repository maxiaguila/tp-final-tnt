package com.example.tp_final_tnt.historial

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.example.tp_final_tnt.R


import com.example.tp_final_tnt.dummy.PlatoHistorial

import kotlinx.android.synthetic.main.fragment_historial_plato.view.*

class HistorialAdapter(
    private val historialListener: HistorialListener
) : RecyclerView.Adapter<HistorialAdapter.ViewHolder>() {



    private lateinit var historial: List<PlatoHistorial>


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_historial_plato, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plato = this.historial[position]
        holder.nombre.text = plato.nombre
        holder.descripcion.text = plato.descripcion
        holder.imagen.setImageResource(plato.imagen as Int)
        holder.esFavorito.isChecked = plato.favorito!!
        holder.esFavorito.setOnCheckedChangeListener { _, isChecked ->
            historialListener.onClickCheckboxFavorito(plato, isChecked)
        }
    }

    internal fun setHistorial(platos: List<PlatoHistorial>) {
        this.historial = platos
    }

    override fun getItemCount(): Int = historial.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val vista: View = view
        val nombre: TextView
        val descripcion: TextView
        val imagen: ImageView
        val esFavorito: CheckBox

        init {
            nombre = vista.plato_historial_nombre
            descripcion = vista.plato_historial_descripcion
            imagen = vista.plato_historial_imagen
             esFavorito = vista.plato_historial_favorito
        }

        override fun toString(): String {
            return super.toString() + " '" + nombre.text + "'"
        }
    }
}
